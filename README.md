Debian Toolbox images
=====================

This repository contains Debian based OCI images for use with
[Toolbox](https://github.com/containers/toolbox).


Password-less sudo
------------------

As these images are supposed to be used with
[Toolbox](https://github.com/containers/toolbox), user is allowed to call any
command with sudo without being prompted for password.

If image is used in another context, this might be inappropriate and a big
security issue!  For other uses, you should consider using standard Debian
container images instead.
